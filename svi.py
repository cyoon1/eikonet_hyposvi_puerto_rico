#dv='cuda:5'
dv='cuda'
# $ gpustat (find which GPUs free)
# $ export CUDA_VISIBLE_DEVICES=5,6,7
# $ echo $CUDA_VISIBLE_DEVICES
# $ CUDA_VISIBLE_DEVICES=5 python svi.py

# EikoNet
from EikoNet import model    as md
from EikoNet import database as db
from EikoNet import plot     as pt 

# HypoSVI
from HypoSVI import location as lc

# Downloading files from Google Drive
#from google_drive_downloader import GoogleDriveDownloader as gdd


# Additional Pacakges for projections etc
from IPython.display import Image
from IPython.display import display
from pyproj import Proj
#import pandas as pd
#import numpy as np
from glob import glob

import math
import numpy as np
import pandas as pd
import torch
import torch.autograd as autograd
import torch.optim as optim
import matplotlib.pylab as plt
import seaborn as sns
import os

PATH = '/atomic-data/cyoon/PuertoRico/Eiko_out'

#xmin               = [-67.5,17.5,-4] #Lat,Long,SDepth
#xmax               = [-65.0,18.5,30] #SDepth
#xmin               = [-67.5,17.5,-0.5] #Lat,Long,Depth
#xmax               = [-65.0,18.5,50]

xmin               = [-67.5,17.5,-2] #Lat,Long,Depth
#xmax               = [-65.0,18.5,20] #Lat,Long,Depth #PRSN
xmax               = [-65.0,18.5,41] #Lat,Long,Depth #ZHANG
##xmin               = [-67.3,17.6,-2] #Lat,Long,Depth #RESTRICT
##xmax               = [-66.4,18.3,41] #Lat,Long,Depth #RESTRICT
##xmin               = [-67.5,17.5,-2] #Lat,Long,Depth #RESTRICTDEPTH
##xmax               = [-65.0,18.5,25] #Lat,Long,Depth #RESTRICTDEPTH
projection         = "+proj=utm +zone=19, +north +ellps=WGS84 +datum=WGS84 +units=km +no_defs"

#vp_dir = 'VP_PRSN'
#vp_file = vp_dir+'/VP_PRSN.csv'
#vp_model_file = 'Model_Epoch_00014_ValLoss_0.006514014532663545.pt'
##vp_model_file = 'Model_Epoch_00014_ValLoss_0.0032528631574705357.pt'
#vs_dir = 'VS_PRSN'
#vs_file = vs_dir+'/VS_PRSN.csv'
#vs_model_file = 'Model_Epoch_00014_ValLoss_0.0060166874305254085.pt'
##vs_model_file = 'Model_Epoch_00014_ValLoss_0.0035651540033575287.pt'
#
##in_event_file = 'EQT_20200107_20200114_REAL_HYPOINVERSE_VELPRSN_combined_real_magcat_locate_pr_PickError010.nlloc'
##out_hyposvi_dir = 'EQT_20200107_20200114_REAL_HYPOINVERSE_VELPRSN_PickError010_Events'
#in_event_file = 'EQT_20180101_20220101_3REAL_HYPOINVERSE_VELPRSN_combined_real_magcat_locate_pr_PickError010.nlloc'
#out_hyposvi_dir = 'CONF68_EQT_20180101_20220101_3REAL_HYPOINVERSE_VELPRSN_PickError010_Events'



vp_dir = 'VP_ZHANG'
vp_file = vp_dir+'/VP_ZHANG.csv'
vp_model_file = 'Model_Epoch_00016_ValLoss_0.005435878705037267.pt'
vs_dir = 'VS_ZHANG'
vs_file = vs_dir+'/VS_ZHANG.csv'
vs_model_file = 'Model_Epoch_00016_ValLoss_0.00243791271897411.pt'

#in_event_file = 'EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr_PickError010.nlloc'
###out_hyposvi_dir = 'EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_PickError010_Events'
###out_hyposvi_dir = 'RESTRICT_EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_PickError010_Events'
###out_hyposvi_dir = 'RESTRICTDEPTH_EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_PickError010_Events'
###out_hyposvi_dir = 'PARTICLE450_EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_PickError010_Events'
###out_hyposvi_dir = 'TTUNCERT_EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_PickError010_Events'
#out_hyposvi_dir = 'CONF68_EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_PickError010_Events'

#in_event_file = 'EQT_20180101_20210601_REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr_PickError010.nlloc'
#out_hyposvi_dir = 'CONF68_EQT_20180101_20210601_REAL_HYPOINVERSE_VELZHANG_PickError010_Events'
#in_event_file = 'EQT_20180101_20211001_REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr_PickError010.nlloc'
#out_hyposvi_dir = 'CONF68_EQT_20180101_20211001_REAL_HYPOINVERSE_VELZHANG_PickError010_Events'
in_event_file = 'EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr_PickError010.nlloc'
out_hyposvi_dir = 'CONF68_EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_PickError010_Events'


# ------------- VP Velocity Model ----------
vm_vp = db.Graded1DVelocity(('{}/'+vp_file).format(PATH),xmin=xmin,xmax=xmax,projection=projection)
model_VP  = md.Model(('{}/'+vp_dir).format(PATH),vm_vp,device=dv)
model_VP.load(('{}/'+vp_dir+'/'+vp_model_file).format(PATH))

# ------------- VS Velocity Model ----------
vm_vs = db.Graded1DVelocity(('{}/'+vs_file).format(PATH),xmin=xmin,xmax=xmax,projection=projection)
model_VS  = md.Model(('{}/'+vs_dir).format(PATH),vm_vs,device=dv)
model_VS.load(('{}/'+vs_dir+'/'+vs_model_file).format(PATH))


# ------ HypoSVI -------
PATH_EVT = PATH+'/'+out_hyposvi_dir
if not os.path.exists(PATH_EVT):
   os.makedirs(PATH_EVT)

EVT = lc.IO_NLLoc2JSON(('{}/'+in_event_file).format(PATH), EVT={}, startEventID=1000000) # from REAL/HYPOINVERSE
lc.IO_JSON('{}/Picks.json'.format(PATH_EVT),Events=EVT,rw_type='w')
EVT  = lc.IO_JSON('{}/Picks.json'.format(PATH_EVT),rw_type='r')
print("len(EVT): ", len(EVT))

# ------------ Loading the station data ------------
Stations       = pd.read_csv('{}/pr_st.out'.format(PATH),sep=r'\s+')
Stations       = Stations.drop_duplicates(['Network','Station'],keep='last').reset_index(drop=True)
Stations

LocMethod = lc.HypoSVI([model_VP,model_VS],Phases=['P','S'],device=dv)
LocMethod.plot_info['EventPlot']['Traces']['Plot Traces']     = False
##LocMethod.plot_info['EventPlot']['Traces']['Trace Host']      = '/atomic-data/cyoon/PuertoRico/downloads_mseeds'
LocMethod.plot_info['EventPlot']['Traces']['Trace Host']      = '/atomic-data/cyoon/PuertoRico/DATA/downloads_mseeds/2020_01'
LocMethod.plot_info['EventPlot']['Traces']['Trace Host Type'] = 'EQTransformer'
LocMethod.plot_info['EventPlot']['Traces']['Channel Types']   = ['EH*','HH*','BH*','HN*']

#!CY
##LocMethod.location_info['Number of Particles']                  = 450
##LocMethod.location_info['Travel Time Uncertainty - [Gradient(km/s),Min(s),Max(s)]'] = [0.2,0.2,4.0]
##LocMethod.location_info['Travel Time Uncertainty - [Gradient(km/s),Min(s),Max(s)]'] = [0.2,0.2,8.0]
LocMethod.location_info['Location Uncertainty Percentile (%)']  = 68.0

##LocMethod.LocateEvents(EVT, Stations, output_plots=True, timer=True, output_path='{}'.format(PATH_EVT))
LocMethod.LocateEvents(EVT, Stations, output_plots=False, timer=True, output_path='{}'.format(PATH_EVT))

LocMethod.CataloguePlot(filepath='{}/CatalogPlot.png'.format(PATH_EVT),Events=lc.IO_JSON('{}/Catalogue.json'.format(PATH_EVT),rw_type='r'),user_xmin=[None,None,None],user_xmax=[None,None,None])
