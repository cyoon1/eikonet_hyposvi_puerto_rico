dv='cuda:2'

# EikoNet
from EikoNet import model    as md
from EikoNet import database as db
from EikoNet import plot     as pt 

## HypoSVI
#from HypoSVI import location as lc
#
## Downloading files from Google Drive
#from google_drive_downloader import GoogleDriveDownloader as gdd
#
#
## Additional Pacakges for projections etc
#from IPython.display import Image
#from IPython.display import display
#from pyproj import Proj
#import pandas as pd
#import numpy as np
from glob import glob

import math
import numpy as np
import pandas as pd
import torch
import torch.autograd as autograd
import torch.optim as optim
import matplotlib.pylab as plt
import seaborn as sns


PATH = '/atomic-data/cyoon/PuertoRico/Eiko_out'

#xmin               = [-67.5,17.5,-4] #Lat,Long,SDepth
#xmax               = [-65.0,18.5,30] # SDepth
#xmin               = [-67.5,17.5,-0.5] #Lat,Long,Depth
#xmax               = [-65.0,18.5,50]

xmin               = [-67.5,17.5,-2] #Lat,Long,Depth
###xmax               = [-65.0,18.5,20] #Lat,Long,Depth #PRSN
xmax               = [-65.0,18.5,41] #Lat,Long,Depth #ZHANG
projection         = "+proj=utm +zone=19, +north +ellps=WGS84 +datum=WGS84 +units=km +no_defs"

###vp_dir = 'VP_PRSN'
###vp_file = vp_dir+'/VP_PRSN.csv'
###vs_dir = 'VS_PRSN'
###vs_file = vs_dir+'/VS_PRSN.csv'

vp_dir = 'VP_ZHANG'
vp_file = vp_dir+'/VP_ZHANG.csv'
vs_dir = 'VS_ZHANG'
vs_file = vs_dir+'/VS_ZHANG.csv'

# ------------- VP Velocity Model ----------
# Loading the CSV for viewing
###VP = pd.read_csv('{}/VP/VP.csv'.format(PATH),names=['Depth(km)','Velocity(km/s)']) # Don't need this line just for looking
#VP = pd.read_csv('{}/'+vp_file.format(PATH),names=['Depth(km)','Velocity(km/s)']) # Don't need this line just for looking

# Defining the Velocity Model region of interest
###vm_vp = db.Graded1DVelocity('{}/VP/VP.csv'.format(PATH),xmin=xmin,xmax=xmax,projection=projection)
vm_vp = db.Graded1DVelocity(('{}/'+vp_file).format(PATH),xmin=xmin,xmax=xmax,projection=projection)

# Plotting the interpolated velocity model
###vm_vp.plot('{}/VP/VP.png'.format(PATH))
vm_vp.plot(('{}/'+vp_dir+'/VP.png').format(PATH))

###model_VP  = md.Model('{}/VP'.format(PATH),vm_vp,device=dv)
model_VP  = md.Model(('{}/'+vp_dir).format(PATH),vm_vp,device=dv)

model_VP.Params['Training']['Number of Epochs']   = 16 # For the 1D velocity 10 epochs could be overkill
model_VP.Params['Training']['Save Every * Epoch'] = 1 # For the 1D velocity 10 epochs could be overkill
model_VP.train()

###vm_vp.plot_TestPoints(model_VP,'{}/VP/VP_TestPoints.png'.format(PATH))
vm_vp.plot_TestPoints(model_VP,('{}/'+vp_dir+'/VP_TestPoints.png').format(PATH))

# VS EikoNet Training

# Constructing a Velocity model function 
###vm_vs = db.Graded1DVelocity('{}/VS/VS.csv'.format(PATH),xmin=xmin,xmax=xmax,projection=projection)
vm_vs = db.Graded1DVelocity(('{}/'+vs_file).format(PATH),xmin=xmin,xmax=xmax,projection=projection)

# Plotting the interpolated velocity model
###vm_vs.plot('{}/VS/VS.png'.format(PATH))
vm_vs.plot(('{}/'+vs_dir+'/VS.png').format(PATH))

###model_VS  = md.Model('{}/VS'.format(PATH),vm_vs,device=dv)
model_VS  = md.Model(('{}/'+vs_dir).format(PATH),vm_vs,device=dv)

model_VS.Params['Training']['Number of Epochs']   = 16 # For the 1D velocity 10 epochs could be overkill
model_VS.Params['Training']['Save Every * Epoch'] = 1 # For the 1D velocity 10 epochs could be overkill
model_VS.train()

###vm_vs.plot_TestPoints(model_VS,'{}/VS/VS_TestPoints.png'.format(PATH))
vm_vs.plot_TestPoints(model_VS,('{}/'+vs_dir+'/VS_TestPoints.png').format(PATH))

